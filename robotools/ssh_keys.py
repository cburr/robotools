from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import getpass
import os

import paramiko

__all__ = [
    'find_local_key'
]

key_files = {
    '~/.ssh/id_rsa': paramiko.rsakey.RSAKey,
    '~/.ssh/id_dsa': paramiko.dsskey.DSSKey,
    '~/.ssh/id_ecdsa': paramiko.ecdsakey.ECDSAKey,
    '~/.ssh/id_ed25519': paramiko.ecdsakey.ECDSAKey
}


def _get_available_keys():
    for key in paramiko.agent.Agent().get_keys():
        yield key

    for filename, key_class in key_files.items():
        filename = os.path.expanduser(filename)
        if not os.path.isfile(filename):
            continue
        try:
            key = key_class(filename=filename)
        except paramiko.ssh_exception.PasswordRequiredException:
            prompt = 'Password for '+filename+': '
            key = key_class(filename=filename, password=getpass.getpass(prompt))
        yield key


def find_local_key(nova_keypairs):
    for pkey in _get_available_keys():
        for nova_keypair in nova_keypairs:
            if nova_keypair.public_key.split()[1] == pkey.get_base64():
                return nova_keypair
    return False
