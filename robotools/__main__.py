from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import random
import string


def run(is_basic=True):
    from .openstack import OpenstackAPI
    from . import ssh_keys
    from . import instance
    from .wizard import get_input, is_valid_size, is_token_valid

    openstack = OpenstackAPI()

    instance_name = '-'.join([
        openstack.username, 'runner',
        ''.join([random.choice(string.ascii_lowercase) for i in range(5)])
    ])
    if is_basic:
        print('Setting default runner name to:', instance_name)
    else:
        instance_name = get_input(
            'Choose a name for the instance.',
            validator='^\\S+$',  # Require the name to not contain whitespace
            default=instance_name
        )
    # TODO Check the instance name isn't taken already
    # TODO Prefix with robotools to mark VMs as removable by this tool?

    instance_flavour = get_input(
        'Choose a flavour for the instance.',
        options=openstack.get_flavours(), skip=is_basic
    )

    image = get_input(
        'Choose a image for the instance.',
        options=openstack.get_images(), skip=is_basic
    )

    if is_basic:
        keypair = ssh_keys.find_local_key(openstack.get_keypairs())
        if not keypair:
            raise NotImplementedError('TODO Need to generate a keypair and register it with openstack')
    else:
        while True:
            keypair = get_input(
                'Choose a keypair for the instance.',
                options=openstack.get_keypairs(),
            )
            print('Validating selected key')
            if ssh_keys.find_local_key([keypair]):
                break
            else:
                print('No corresponding private key found, please retry.')

    gitlab_token = get_input(
        'Enter the runner registration token for your repository. This can be '
        'obtained from step 3 on: \n'
        'https://gitlab.cern.ch/{username}/{repository_name}/settings/ci_cd',
        validator=is_token_valid
    )

    use_cvmfs = get_input(
        'Enable CVMFS?',
        options=['Yes', 'No'], default='Yes', skip=is_basic
    )
    use_cvmfs = use_cvmfs == 'Yes'

    privileged = get_input(
        'Enable privileged mode for the docker runner? This is required for '
        'running docker inside the runner.',
        options=['Yes', 'No'], default='Yes', skip=is_basic
    )
    privileged = privileged == 'Yes'

    use_data_volume = get_input(
        'Mount a /data to share objects between runners, proceed?',
        options=['Yes', 'No'], default='Yes', skip=is_basic
    )
    if use_data_volume == 'Yes':
        data_volume_size = int(get_input(
            'Choose a size for the /data volume in GB:',
            validator=is_valid_size, default=100, skip=is_basic
        ))
    else:
        data_volume_size = None

    # TODO Keytab?? Or copy data??
    # TODO Resources?

    proceed = get_input(
        'About to create the sever, proceed?',
        options=['Yes', 'No']
    )
    if proceed == 'Yes':
        instance.create(
            openstack, instance_name, image, instance_flavour, keypair,
            gitlab_token, use_cvmfs, data_volume_size, privileged
        )
    else:
        print('Aborting...')


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Create an GitLab runner on a openstack VM')
    parser.add_argument('--advanced', action='store_true')
    args = parser.parse_args()
    run(is_basic=not args.advanced)
