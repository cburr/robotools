from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from os.path import join
import sys
from tempfile import NamedTemporaryFile
from time import sleep

from jinja2 import Environment, PackageLoader
import paramiko

__all__ = [
    'create'
]

env = Environment(loader=PackageLoader('robotools'))
gitlab_ci_api = 'https://gitlab.cern.ch/ci'


def create(api, name, image, flavour, keypair, gitlab_token, use_cvmfs,
           data_volume_size, privileged):
    # Create the data volume if requested
    if data_volume_size is None:
        block_device_mapping = None
    else:
        assert data_volume_size > 0
        volume = api.create_volume(data_volume_size)
        block_device_mapping = [{
            'boot_index': None,
            'uuid': volume.id,
            'source_type': 'volume',
            'destination_type': 'volume',
            'delete_on_termination': False
        }]

    # Create the server
    server = api.create_server(
        name, image, flavour, key_name=keypair.name,
        block_device_mapping_v2=block_device_mapping
    )

    # Wait for the server to build
    print('Waiting for the server to build')
    previous_status = current_status = server.status
    while current_status != 'ACTIVE':
        # Log output whenever the status changes
        if current_status != previous_status:
            print(' - Status changed to', current_status)
            previous_status = current_status
        sleep(5)
        current_status = server.status

    # Ensure the volume has been mounted if required
    if data_volume_size is None:
        data_volume_device = None
        data_volume_mount_point = None
    else:
        error_message = 'Failed to mount ' + str(volume) + ' on ' + str(server)
        if not volume.servers:
            raise RuntimeError(error_message)
        mounted_server, data_volume_device = volume.servers[0]
        assert mounted_server == server
        data_volume_mount_point = '/data'

    # Open an ssh connection to the new VM
    print('VM active, waiting for ssh daemon to start')
    while True:
        try:
            client = paramiko.SSHClient()
            client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            client.load_system_host_keys()
            client.connect(server.hostname, username='root')
            break
        except paramiko.ssh_exception.NoValidConnectionsError:
            pass
        except paramiko.ssh_exception.AuthenticationException:
            # Sometimes this happens if we try to connect too soon
            print('Warning: Authentication failed, retrying')
        sleep(5)

    # Upload the files we need
    files_to_copy = ['Dockerfile', 'docker-daemon.json', 'setup_runner.sh', 'setup_runner.sh']
    if data_volume_size is not None:
        files_to_copy.append('format_and_mount_volume.sh')

    sftp_client = client.open_sftp()
    for fn in files_to_copy:
        with NamedTemporaryFile() as f:
            # Use jinja2 to format the template
            f.write(env.get_template(fn).render(
                instance_name=name,
                gitlab_ci_api=gitlab_ci_api,
                gitlab_token=gitlab_token,
                use_cvmfs=use_cvmfs,
                data_volume_device=data_volume_device,
                data_volume_mount_point=data_volume_mount_point,
                privileged=privileged
            ))
            # Flush to ensure the file actually contains the data
            f.flush()
            sftp_client.put(f.name, join('/root', fn))

    sftp_client.close()

    # Set up the data volume mount if required
    if data_volume_device:
        run_command(client, 'bash /root/format_and_mount_volume.sh', name)
    # Set up the gitlab runner
    run_command(client, 'bash /root/setup_runner.sh', name)
    # And we're done
    client.close()
    print('Created', name, 'successfully')


def run_command(client, command, server_name):
    # Run the set up script
    chan = client.get_transport().open_session()
    chan.set_combine_stderr(True)
    chan.exec_command(command)
    stdout = chan.makefile('r')
    line = stdout.readline()
    # TODO Log to file as well?
    while line:
        print(line, end='')
        line = stdout.readline()

    return_code = chan.recv_exit_status()
    if return_code != 0:
        print('Remotely ran command "'+command+'" returned: '+str(return_code))
        print('This was probably caused by a random error, retrying may work.')
        print('The failed machine can be removed by running:')
        print(' $ openstack server delete', server_name)
        # TODO Clean up volumes?
        print()
        print('If this error is seen repeatedly, please report an issue at: ',
              'https://gitlab.cern.ch/cburr/robotools/issues')
        sys.exit(1)
