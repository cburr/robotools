#!/usr/bin/env bash
# Setup bash strict mode
set -euo pipefail
IFS=$'\n\t'
# Echo each command before executing
set -x

# Update the VM
yum -y update
yum install -y yum-utils
yum install -y parted

# Create a partition
parted {{ data_volume_device }} --script -- mklabel gpt mkpart primary 0% 100%
# Sleep else mkfs.xfs is likely to fail
sleep 5
# Format the partition
mkfs.xfs {{ data_volume_device }}1

# Make the mount point
mkdir {{ data_volume_mount_point}}

# Add to /etc/fstab to auto mount on boot
# Set the DEVNAME, UUID and TYPE environment variables
blkid -o export {{ data_volume_device }}1
eval $(blkid -o export {{ data_volume_device }}1)
cat /etc/fstab
echo "UUID=${UUID}    {{ data_volume_mount_point}}    ${TYPE}    defaults" >> /etc/fstab
cat /etc/fstab

# Test the mount
mount {{ data_volume_mount_point }}
ls -la {{ data_volume_mount_point }}
df -h
