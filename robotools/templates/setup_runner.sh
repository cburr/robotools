#!/usr/bin/env bash
# Setup bash strict mode
set -euo pipefail
IFS=$'\n\t'
# Echo each command before executing
set -x

# Update the VM
yum -y update
yum install -y yum-utils

# Install docker
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum makecache fast
yum install -y docker-ce

# Enable and start docker
systemctl enable docker
systemctl start docker

# Workaround for Google's DNS servers being blocked at CERN
# https://robinwinslow.uk/2016/06/23/fix-docker-networking-dns/
mv /root/docker-daemon.json /etc/docker/daemon.json
systemctl restart docker

# Set up locmap
# TODO Setup CERNBOX?
yum -y install locmap
{%- if use_cvmfs %}
locmap --enable cvmfs
locmap --configure cvmfs
cvmfs_config probe
{% endif %}

# Build the default container
mkdir ~/default-analysis-container
cd ~/default-analysis-container
mv /root/Dockerfile Dockerfile
docker build --tag default-analysis-container .
cd -

# Set up the gitlab runner
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v /etc/gitlab-runner:/etc/gitlab-runner \
  {%- if use_cvmfs %}-v /cvmfs:/cvmfs \{% endif %}
  {%- if data_volume_mount_point %}-v {{ data_volume_mount_point }}:{{ data_volume_mount_point }} \{% endif %}
  {%- if privileged %}--privileged \{% endif %}
  gitlab/gitlab-runner:v1.11.2

# Register the repository with the runner
docker exec -i gitlab-runner gitlab-runner register -n \
    --url "{{ gitlab_ci_api }}" --registration-token "{{ gitlab_token }}" \
    --description "{{ instance_name }}" --executor "docker"  \
    {%- if use_cvmfs %}--docker-volumes /cvmfs:/cvmfs \{% endif %}
    {%- if data_volume_mount_point %}--docker-volumes {{ data_volume_mount_point }}:{{ data_volume_mount_point }} \{% endif %}
    {%- if privileged %}--docker-privileged \{% endif %}
    --docker-pull-policy always \
    --docker-image "default-analysis-container" --docker-pull-policy "if-not-present"

{% if privileged -%}
# We have to remove the search of cern.ch in /etc/resolv.conf if in privileged mode
# else the connection to tcp://docker:2375 tried to resolve to tcp://docker.cern.ch:2375
# See: https://gitlab.com/gitlab-org/gitlab-ci-multi-runner/issues/2064
sed -i.bak 's/search/; search/' /etc/resolv.conf
# Prevent the changes from being overwritten on boot
chattr +i /etc/resolv.conf
{%- endif -%}
