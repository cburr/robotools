from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import datetime
import re

from keystoneauth1 import session as keystone_session
from os_client_config import config as cloud_config
from openstackclient.api import auth
from keystoneclient.v3 import client as keystone_client
from glanceclient import client as glance_client
from novaclient import client as nova_client

from . import wizard


__all__ = [
    'Server',
    'Volume',
    'OpenstackAPI'
]


class Server(object):
    def __init__(self, nova, server_id=None, server_name=None):
        self._nova = nova
        if server_id and not server_name:
            self._id = server_id
        elif server_name and not server_id:
            servers = [s for s in nova.servers.list() if s.name == server_name]
            if len(servers) != 1:
                raise ValueError(servers)
            self._id = servers[0].id
        else:
            raise RuntimeError(
                'Either `server_id` or `server_name` must be specified')

    def __str__(self):
        return repr(self.server)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._id == other._id
        else:
            return False

    @property
    def server(self):
        # TODO Make more efficient?
        return self._nova.servers.get(self._id)

    @property
    def id(self):
        return self._id

    @property
    def status(self):
        return self.server.status

    @property
    def hostname(self):
        return self.server.name+'.cern.ch'


class Volume(object):
    def __init__(self, nova, volume_id):
        self._id = volume_id
        self._nova = nova

    def __str__(self):
        return repr(self.volume)

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._id == other._id
        else:
            return False

    @property
    def volume(self):
        # TODO Make more efficient?
        return self._nova.volumes.get(self._id)

    @property
    def servers(self):
        """Get the server associated with this volume and the mount point"""
        volume = self.volume
        # TODO Do we want to support being attached to multiple servers?
        assert len(volume.attachments) <= 1
        return [
            (Server(self._nova, server_id=attach_info['server_id']),
             attach_info['device'])
            for attach_info in volume.attachments
        ]

    @property
    def id(self):
        return self._id


class OpenstackAPI(object):
    def __init__(self, project_name=None):
        # Set up authentication, based on:
        # https://github.com/iahmad-khan/ai-tools/blob/master/src/aitools/openstack_auth_client.py
        cloud = cloud_config.OpenStackConfig().get_one_cloud()
        auth.get_plugin_list()
        auth_plugin_name = auth.select_auth_plugin(cloud)
        auth_plugin, auth_params = auth.build_auth_params(auth_plugin_name, cloud)
        session = keystone_session.Session(
            auth=auth_plugin.load_from_options(**auth_params))
        keystone = keystone_client.Client(session=session)

        # User information
        username = session.get_user_id()
        projects = keystone.projects.list(user=username)

        # Select a project
        if project_name is None:
            if len(projects) == 1:
                project_name = projects[0].name
            else:
                project = wizard.get_input(
                    'Choose a project to work with.',
                    options=projects, default=auth_params['project_name']
                )
                project_name = project.name

        if project_name not in [p.name for p in projects]:
            raise ValueError('Invalid project name:' + project_name, projects)

        # Create a new session with the correct project name
        auth_params['project_name'] = project_name
        session = keystone_session.Session(
            auth=auth_plugin.load_from_options(**auth_params))
        keystone = keystone_client.Client(session=session)

        # Set up clients
        glance = glance_client.Client(version=1, session=session)
        nova = nova_client.Client(version=2, session=session)

        # TODO Perform client checking using something other than assert
        assert keystone.regions.list()
        assert glance.images.list()
        assert nova.keypairs.list()

        self._username = username
        self._project_name = project_name
        self._nova = nova
        self._glance = glance

    @property
    def username(self):
        return self._username

    def get_keypairs(self):
        """Return a list of keypairs"""
        # TODO Should we check if the key is available
        return self._nova.keypairs.list()

    def get_flavours(self):
        """Return a list of instance flavours, sorted by ram size"""
        return sorted([
            f for f in self._nova.flavors.list()
            if not getattr(f, 'OS-FLV-DISABLED:disabled')
        ], key=lambda f: f.ram)

    def get_images(self, image_regex='CC7 - x86_64 \[(\\d+)-(\\d+)-(\\d+)\]'):
        """Return a list of images that match the `image_regex` sorted by time"""
        assert image_regex == 'CC7 - x86_64 \[(\\d+)-(\\d+)-(\\d+)\]'
        return sorted(
            [i for i in self._glance.images.list() if re.match(image_regex, i.name)],
            key=lambda i: datetime.strptime(i.name, 'CC7 - x86_64 [%Y-%m-%d]')
        )

    def create_volume(self, size, name=None):
        # Set a default name
        if name is None:
            max_int = -1
            for v in self._nova.volumes.list():
                match = re.match('robotools-volume-(\\d+)', v.display_name)
                if not match:
                    continue
                max_int = max(max_int, int(match.groups()[0]))
            name = 'robotools-volume-{}'.format(max_int+1)

        volume = self._nova.volumes.create(size, display_name=name)
        return Volume(self._nova, volume.id)

    def create_server(self, name, image, flavor, *args, **kwargs):
        server = self._nova.servers.create(name, image, flavor, *args, **kwargs)
        return Server(self._nova, server_id=server.id)
