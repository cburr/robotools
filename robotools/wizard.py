from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from termios import tcflush, TCIOFLUSH
import re
import sys

import six
import requests

if sys.version_info < (3, 0):
    input = raw_input  # NOQA

__all__ = [
    'get_input',
    'is_token_valid',
    'is_valid_size'
]

gitlab_ci_api = 'https://gitlab.cern.ch/ci'


def get_input(title, options=None, validator='.+', default=None, skip=False):
    # Flush stdin
    tcflush(sys.stdin, TCIOFLUSH)

    # If validator is a string, treat it as regex
    if isinstance(validator, six.string_types):
        validator = lambda s, regex=validator: re.match(regex, s)

    if options is None:
        if not skip:
            print(title+':')
            if default:
                print('    - ', default, '[default]')
        _options = None
    else:
        # There should always be at least one option
        assert options

        # Use the last option by default
        if default is None:
            default = options[-1]
            if not isinstance(default, six.string_types):
                default = default.name

        # _options will be a dictionary of {string: value}
        _options = {}
        if not skip:
            print(title+' Available options are:')
        for option in options:
            if isinstance(option, six.string_types):
                _options[option] = option
            else:
                _options[option.name] = option
                option = option.name

            if not skip:
                print('    - "'+option+'"', end='')
                print(' [default]' if option == default else '')

    while True:
        if skip:
            # To skip user input there must be a default
            assert default
            user_input = default
        else:
            user_input = input(' > ')

        # Apply the default if nothing was typed
        if default and user_input == '':
            user_input = default

        if not validator(user_input):
            pass
        elif _options and user_input not in _options:
            # The input doesn't match one of the allowed options
            pass
        elif _options and user_input in _options:
            return _options[user_input]
        else:
            return user_input
        print('Invalid input, retry')


def is_token_valid(gitlab_token):
    """Try to create a dummy runner to validate the token."""
    # See: https://docs.gitlab.com/ee/api/ci/runners.html
    response = requests.post(
        gitlab_ci_api+'/api/v1/runners/register',
        data={'token': gitlab_token}
    )
    if response.status_code == 201:
        runner_token = response.json()['token']
    elif response.status_code in [400, 403]:
        return False
    else:
        print(response)
        print(response.json())
        raise RuntimeError('Something went wrong parsing token')

    # Tidy up the dummy runner that was added
    response = requests.delete(
        gitlab_ci_api+'/api/v1/runners/delete',
        data={'token': runner_token}
    )
    if response.status_code != 200:
        print('WARN: Failed to clean up the runner!')
        print(response.status_code)
        print(response.json())

    return True


def is_valid_size(size):
    try:
        size = int(size)
    except ValueError:
        return False
    else:
        return size > 0
