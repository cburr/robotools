# robotools

## Usage

```bash
ssh lxplus7.cern.ch
git clone https://:@gitlab.cern.ch:8443/cburr/robotools.git
cd robotools
/usr/bin/python -m pip install --target robotools_pip paramiko
PYTHONPATH=$PWD/robotools_pip /usr/bin/python -m robotools [--advanced]
```
